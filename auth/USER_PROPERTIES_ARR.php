<?
$arResult "USER_PROPERTIES"  "DATA"  = array(
	"UF_FULLNAMECOMPANY" => array(		"ID" => 7,
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_FULLNAMECOMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "1",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => "" ), "EDIT_FORM_LABEL" => "Полное наименование",
		"LIST_COLUMN_LABEL" => "Полное наименование",
		"LIST_FILTER_LABEL" => "Полное наименование",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit",
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			), ), "VALUE" => false, "~EDIT_FORM_LABEL" => "Полное наименование"
	),
	"UF_SHORTNAMECOMPANY" => array(
		"ID" => "8",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_SHORTNAMECOMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "2",
		"MULTIPLE" => "N",
		"MANDATORY" => "N",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => "" 
		),
		"EDIT_FORM_LABEL" => "Сокращенное наименование",
		"LIST_COLUMN_LABEL" => "Сокращенное наименование",
		"LIST_FILTER_LABEL" => "Сокращенное наименование",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Сокращенное наименование",
	),
	"UF_OGRN" => array(
		"ID" => "9",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_OGRN",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "3",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "ОГРН",
		"LIST_COLUMN_LABEL" => "ОГРН",
		"LIST_FILTER_LABEL" => "ОГРН",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "ОГРН"
	),
	"UF_OGRNIP" => array(
		"ID" => "16",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_OGRNIP",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "3",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "ОГРНИП",
		"LIST_COLUMN_LABEL" => "ОГРНИП",
		"LIST_FILTER_LABEL" => "ОГРНИП",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "ОГРНИП"
	),
	"UF_DATA_REG_REESTR" => array(
		"ID" =>"10",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_DATA_REG_REESTR",
		"USER_TYPE_ID" => "datetime",
		"XML_ID" => "",
		"SORT" => "4",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => array(
				"TYPE" => "FIXED",
				"VALUE" => false,
			),
			"USE_SECOND" => "Y"
		),
		"EDIT_FORM_LABEL" => "Дата внесения в Реестр",
		"LIST_COLUMN_LABEL" => "Дата внесения в Реестр",
		"LIST_FILTER_LABEL" => "Дата внесения в Реестр",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "datetime",
			"CLASS_NAME" => "CUserTypeDateTime",
			"DESCRIPTION" => "Дата со временем",
			"BASE_TYPE" => "datetime",
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeDateTime",
				1 => "GetPublicView"
			),
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeDateTime",
				1 => "GetPublicEdit"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Дата внесения в Реестр"
	),
	"UF_ADDR_COMPANY" => array(
		"ID" =>"12",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_ADDR_COMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "5",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "Юридический адрес",
		"LIST_COLUMN_LABEL" => "Юридический адрес",
		"LIST_FILTER_LABEL" => "Юридический адрес",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Юридический адрес"
	),
	"UF_POSTBOX_ADDR" => array(
		"ID" =>"13",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_POSTBOX_ADDR",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "6",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "Почтовый адрес",
		"LIST_COLUMN_LABEL" => "Почтовый адрес",
		"LIST_FILTER_LABEL" => "Почтовый адрес",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Почтовый адрес"
	),
	"UF_INN" => array(
		"ID" =>"11",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_INN",
		"USER_TYPE_ID" => "integer",
		"XML_ID" => "",
		"SORT" => "7",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"MIN_VALUE" => 0,
			"MAX_VALUE" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "ИНН",
		"LIST_COLUMN_LABEL" => "ИНН",
		"LIST_FILTER_LABEL" => "ИНН",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "integer",
			"CLASS_NAME" => "CUserTypeInteger",
			"DESCRIPTION" => "Целое число",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeInteger",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeInteger",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "ИНН"
	),
	"UF_MOB_PHONE_COMPANY" => array(
		"ID" =>"15",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MOB_PHONE_COMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "9",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "+7 (900) 000-00-00",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "Мобильный телефон",
		"LIST_COLUMN_LABEL" => "Мобильный телефон",
		"LIST_FILTER_LABEL" => "Мобильный телефон",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "Строка",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Мобильный телефон"
	),
	"UF_AGREE_MESS" => array(
		"ID" => "4",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_AGREE_MESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Согласен (согласна) получать уведомления по электронной почте",
		"LIST_COLUMN_LABEL" => "Согласен (согласна) получать уведомления по электронной почте",
		"LIST_FILTER_LABEL" => "Согласен (согласна) получать уведомления по электронной почте",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "Да/Нет",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Согласен (согласна) получать уведомления по электронной почте"
	),
	"UF_MAILTOMYMAIL" => array(
		"ID" => "5",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILTOMYMAIL",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "N",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email совпадает с основным email пользователя",
		"LIST_COLUMN_LABEL" => "Email совпадает с основным email пользователя",
		"LIST_FILTER_LABEL" => "Email совпадает с основным email пользователя",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "Да/Нет",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email совпадает с основным email пользователя"
	),
	"UF_MAILFORMESS" => array(
		"ID" => "6",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILFORMESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email для получения уведомлений",
		"LIST_COLUMN_LABEL" => "Email для получения уведомлений",
		"LIST_FILTER_LABEL" => "Email для получения уведомлений",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "Да/Нет",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email для получения уведомлений"
	),
   "UF_AGREE_MYDATA" => array(
     "ID" => "17",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_MYDATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
     "EDIT_FORM_LABEL" => "Согласен (согласна) на обработку персональных данных",
     "LIST_COLUMN_LABEL" => "Согласен (согласна) на обработку персональных данных",
     "LIST_FILTER_LABEL" => "Согласен (согласна) на обработку персональных данных",
     "ERROR_MESSAGE" => "",
     "HELP_MESSAGE" => "",
     "USER_TYPE" => array(
       "USER_TYPE_ID" => "boolean",
       "CLASS_NAME" => "CUserTypeBoolean",
       "DESCRIPTION" => "Да/Нет",
       "BASE_TYPE" => "int",
       "EDIT_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicEdit"
      ),
       "VIEW_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicView"
      )
    ),
     "VALUE" => false,
     "~EDIT_FORM_LABEL" => "Согласен (согласна) на обработку персональных данных"
  ),
   "UF_AGREE_E_DATA" => array(
     "ID" => "18",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_E_DATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
	"EDIT_FORM_LABEL" => "Согласен (согласна) на передачу и обработку информации в электронном виде",
	"LIST_COLUMN_LABEL" => "Согласен (согласна) на передачу и обработку информации в электронном виде",
	"LIST_FILTER_LABEL" => "Согласен (согласна) на передачу и обработку информации в электронном виде",
	"ERROR_MESSAGE" => "",
	"HELP_MESSAGE" => "",
	"USER_TYPE" => array(
		"USER_TYPE_ID" => "boolean",
		"CLASS_NAME" => "CUserTypeBoolean",
		"DESCRIPTION" => "Да/Нет",
		"BASE_TYPE" => "int",
		"EDIT_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicEdit"
		),
		"VIEW_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicView"
      )
    ),
	"VALUE" => false,
	"~EDIT_FORM_LABEL" => "Согласен (согласна) на передачу и обработку информации в электронном виде"
  )
);