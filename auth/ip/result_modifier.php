<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["USER_PROPERTIES"]["DATA"] = array(
	"UF_FULLNAMECOMPANY" => array(
		"ID" => 7,
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_FULLNAMECOMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "1",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => "" ), "EDIT_FORM_LABEL" => "������ ������������",
		"LIST_COLUMN_LABEL" => "������ ������������",
		"LIST_FILTER_LABEL" => "������ ������������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit",
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			), ), "VALUE" => false, "~EDIT_FORM_LABEL" => "������ ������������"
	),
	"UF_SHORTNAMECOMPANY" => array(
		"ID" => "8",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_SHORTNAMECOMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "2",
		"MULTIPLE" => "N",
		"MANDATORY" => "N",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => "" 
		),
		"EDIT_FORM_LABEL" => "����������� ������������",
		"LIST_COLUMN_LABEL" => "����������� ������������",
		"LIST_FILTER_LABEL" => "����������� ������������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "����������� ������������",
	),
	"UF_OGRNIP" => array(
		"ID" => "16",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_OGRNIP",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "3",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "������",
		"LIST_COLUMN_LABEL" => "������",
		"LIST_FILTER_LABEL" => "������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
					0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "������"
	),
	"UF_DATA_REG_REESTR" => array(
		"ID" =>"10",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_DATA_REG_REESTR",
		"USER_TYPE_ID" => "datetime",
		"XML_ID" => "",
		"SORT" => "4",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => array(
				"TYPE" => "FIXED",
				"VALUE" => false,
			),
			"USE_SECOND" => "Y"
		),
		"EDIT_FORM_LABEL" => "���� �������� � ������",
		"LIST_COLUMN_LABEL" => "���� �������� � ������",
		"LIST_FILTER_LABEL" => "���� �������� � ������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "datetime",
			"CLASS_NAME" => "CUserTypeDateTime",
			"DESCRIPTION" => "���� �� ��������",
			"BASE_TYPE" => "datetime",
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeDateTime",
				1 => "GetPublicView"
			),
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeDateTime",
				1 => "GetPublicEdit"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "���� �������� � ������"
	),
	"UF_ADDR_COMPANY" => array(
		"ID" =>"12",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_ADDR_COMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "5",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "����������� �����",
		"LIST_COLUMN_LABEL" => "����������� �����",
		"LIST_FILTER_LABEL" => "����������� �����",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "����������� �����"
	),
	"UF_POSTBOX_ADDR" => array(
		"ID" =>"13",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_POSTBOX_ADDR",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "6",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "�������� �����",
		"LIST_COLUMN_LABEL" => "�������� �����",
		"LIST_FILTER_LABEL" => "�������� �����",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "�������� �����"
	),
	"UF_INN" => array(
		"ID" =>"11",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_INN",
		"USER_TYPE_ID" => "integer",
		"XML_ID" => "",
		"SORT" => "7",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"MIN_VALUE" => 0,
			"MAX_VALUE" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "���",
		"LIST_COLUMN_LABEL" => "���",
		"LIST_FILTER_LABEL" => "���",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "integer",
			"CLASS_NAME" => "CUserTypeInteger",
			"DESCRIPTION" => "����� �����",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeInteger",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeInteger",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "���"
	),
	"UF_MOB_PHONE_COMPANY" => array(
		"ID" =>"15",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MOB_PHONE_COMPANY",
		"USER_TYPE_ID" => "string",
		"XML_ID" => "",
		"SORT" => "9",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"SIZE" => 20,
			"ROWS" => 1,
			"REGEXP" => "+7 (900) 000-00-00",
			"MIN_LENGTH" => 0,
			"MAX_LENGTH" => 0,
			"DEFAULT_VALUE" => ""
		),
		"EDIT_FORM_LABEL" => "��������� �������",
		"LIST_COLUMN_LABEL" => "��������� �������",
		"LIST_FILTER_LABEL" => "��������� �������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "string",
			"CLASS_NAME" => "CUserTypeString",
			"DESCRIPTION" => "������",
			"BASE_TYPE" => "string",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeString",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "��������� �������"
	),
	"UF_AGREE_MESS" => array(
		"ID" => "4",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_AGREE_MESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"LIST_COLUMN_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"LIST_FILTER_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����"
	),
	"UF_MAILTOMYMAIL" => array(
		"ID" => "5",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILTOMYMAIL",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "N",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email ��������� � �������� email ������������",
		"LIST_COLUMN_LABEL" => "Email ��������� � �������� email ������������",
		"LIST_FILTER_LABEL" => "Email ��������� � �������� email ������������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email ��������� � �������� email ������������"
	),
	"UF_MAILFORMESS" => array(
		"ID" => "6",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILFORMESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email ��� ��������� �����������",
		"LIST_COLUMN_LABEL" => "Email ��� ��������� �����������",
		"LIST_FILTER_LABEL" => "Email ��� ��������� �����������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email ��� ��������� �����������"
	),
	   "UF_AGREE_MYDATA" => array(
     "ID" => "17",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_MYDATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
     "EDIT_FORM_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "LIST_COLUMN_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "LIST_FILTER_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "ERROR_MESSAGE" => "",
     "HELP_MESSAGE" => "",
     "USER_TYPE" => array(
       "USER_TYPE_ID" => "boolean",
       "CLASS_NAME" => "CUserTypeBoolean",
       "DESCRIPTION" => "��/���",
       "BASE_TYPE" => "int",
       "EDIT_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicEdit"
      ),
       "VIEW_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicView"
      )
    ),
     "VALUE" => false,
     "~EDIT_FORM_LABEL" => "�������� (��������) �� ��������� ������������ ������"
  ),
   "UF_AGREE_E_DATA" => array(
     "ID" => "18",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_E_DATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
	"EDIT_FORM_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"LIST_COLUMN_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"LIST_FILTER_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"ERROR_MESSAGE" => "",
	"HELP_MESSAGE" => "",
	"USER_TYPE" => array(
		"USER_TYPE_ID" => "boolean",
		"CLASS_NAME" => "CUserTypeBoolean",
		"DESCRIPTION" => "��/���",
		"BASE_TYPE" => "int",
		"EDIT_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicEdit"
		),
		"VIEW_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicView"
      )
    ),
	"VALUE" => false,
	"~EDIT_FORM_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����"
  )
);

?>