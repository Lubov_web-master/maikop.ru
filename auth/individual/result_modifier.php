<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// ������ ������� ���������� �����
$arResult['SHOW_FIELDS'] = array(
	'������ ���������' => array(
							'LAST_NAME',
							'NAME',
							'SECOND_NAME',
							'PERSONAL_GENDER',
							'LOGIN',
							'PASSWORD',
							'CONFIRM_PASSWORD'
						),
	'���������� ������' => array(
							'EMAIL',
							'PERSONAL_PHONE',
						)
);

$arResult["USER_PROPERTIES"]["DATA"] = array(
	"UF_AGREE_MESS" => array(
		"ID" => "4",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_AGREE_MESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"LIST_COLUMN_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"LIST_FILTER_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "�������� (��������) �������� ����������� �� ����������� �����"
	),
	"UF_MAILTOMYMAIL" => array(
		"ID" => "5",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILTOMYMAIL",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "N",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email ��������� � �������� email ������������",
		"LIST_COLUMN_LABEL" => "Email ��������� � �������� email ������������",
		"LIST_FILTER_LABEL" => "Email ��������� � �������� email ������������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email ��������� � �������� email ������������"
	),
	"UF_MAILFORMESS" => array(
		"ID" => "6",
		"ENTITY_ID" => "USER",
		"FIELD_NAME" => "UF_MAILFORMESS",
		"USER_TYPE_ID" => "boolean",
		"XML_ID" => "",
		"SORT" => "100",
		"MULTIPLE" => "N",
		"MANDATORY" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_IN_LIST" => "Y",
		"EDIT_IN_LIST" => "Y",
		"IS_SEARCHABLE" => "N",
		"SETTINGS" => array(
			"DEFAULT_VALUE" => "1",
			"DISPLAY" => "CHECKBOX",
			"LABEL" => array(
				0 => "",
				1 => ""
			),
			"LABEL_CHECKBOX" => ""
		),
		"EDIT_FORM_LABEL" => "Email ��� ��������� �����������",
		"LIST_COLUMN_LABEL" => "Email ��� ��������� �����������",
		"LIST_FILTER_LABEL" => "Email ��� ��������� �����������",
		"ERROR_MESSAGE" => "",
		"HELP_MESSAGE" => "",
		"USER_TYPE" => array(
			"USER_TYPE_ID" => "boolean",
			"CLASS_NAME" => "CUserTypeBoolean",
			"DESCRIPTION" => "��/���",
			"BASE_TYPE" => "int",
			"EDIT_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicEdit"
			),
			"VIEW_CALLBACK" => array(
				0 => "CUserTypeBoolean",
				1 => "GetPublicView"
			)
		),
		"VALUE" => false,
		"~EDIT_FORM_LABEL" => "Email ��� ��������� �����������"
	),
	"UF_AGREE_MYDATA" => array(
     "ID" => "17",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_MYDATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
     "EDIT_FORM_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "LIST_COLUMN_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "LIST_FILTER_LABEL" => "�������� (��������) �� ��������� ������������ ������",
     "ERROR_MESSAGE" => "",
     "HELP_MESSAGE" => "",
     "USER_TYPE" => array(
       "USER_TYPE_ID" => "boolean",
       "CLASS_NAME" => "CUserTypeBoolean",
       "DESCRIPTION" => "��/���",
       "BASE_TYPE" => "int",
       "EDIT_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicEdit"
      ),
       "VIEW_CALLBACK" => array(
         0 => "CUserTypeBoolean",
         1 => "GetPublicView"
      )
    ),
     "VALUE" => false,
     "~EDIT_FORM_LABEL" => "�������� (��������) �� ��������� ������������ ������"
  ),
   "UF_AGREE_E_DATA" => array(
     "ID" => "18",
     "ENTITY_ID" => "USER",
     "FIELD_NAME" => "UF_AGREE_E_DATA",
     "USER_TYPE_ID" => "boolean",
     "XML_ID" => "",
     "SORT" => "100",
     "MULTIPLE" => "N",
     "MANDATORY" => "N",
     "SHOW_FILTER" => "N",
     "SHOW_IN_LIST" => "Y",
     "EDIT_IN_LIST" => "Y",
     "IS_SEARCHABLE" => "N",
     "SETTINGS" => array(
       "DEFAULT_VALUE" => "1",
       "DISPLAY" => "CHECKBOX",
       "LABEL" => array(
         0 => "",
         1 => ""
      ),
       "LABEL_CHECKBOX" => ""
    ),
	"EDIT_FORM_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"LIST_COLUMN_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"LIST_FILTER_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����",
	"ERROR_MESSAGE" => "",
	"HELP_MESSAGE" => "",
	"USER_TYPE" => array(
		"USER_TYPE_ID" => "boolean",
		"CLASS_NAME" => "CUserTypeBoolean",
		"DESCRIPTION" => "��/���",
		"BASE_TYPE" => "int",
		"EDIT_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicEdit"
		),
		"VIEW_CALLBACK" => array(
			0 => "CUserTypeBoolean",
			1 => "GetPublicView"
      )
    ),
	"VALUE" => false,
	"~EDIT_FORM_LABEL" => "�������� (��������) �� �������� � ��������� ���������� � ����������� ����"
  )
);

?>