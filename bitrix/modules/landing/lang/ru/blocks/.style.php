<?
$MESS["LANDING_BLOCK_STYLE_TEXT_ALIGN"] = "������������ ������";
$MESS["LANDING_BLOCK_STYLE_ROW_ALIGN"] = "������������ ���������";
$MESS["LANDING_BLOCK_STYLE_TEXT_COLOR"] = "���� ������";
$MESS["LANDING_BLOCK_STYLE_BACKGROUND_COLOR"] = "���� ����";
$MESS["LANDING_BLOCK_STYLE_BACKGROUND_GRADIENT"] = "���� ���� (��������)";
$MESS["LANDING_BLOCK_STYLE_BORDER_COLOR"] = "���� �����";
$MESS["LANDING_BLOCK_STYLE_FONT_SIZE"] = "������ ������";
$MESS["LANDING_BLOCK_STYLE_FONT_FAMILY"] = "�����";
$MESS["LANDING_BLOCK_STYLE_TEXT_TRANSFORM"] = "����������";
$MESS["LANDING_BLOCK_STYLE_LINE_HEIGHT"] = "�����������";
$MESS["LANDING_BLOCK_STYLE_LETTER_SPACING"] = "��������";
$MESS["LANDING_BLOCK_STYLE_TEXT_SHADOW"] = "���� ������";
$MESS["LANDING_BLOCK_STYLE_SHADOW"] = "���� �����";
$MESS["LANDING_BLOCK_STYLE_SHADOW_NAME"] = "���� �";
$MESS["LANDING_BLOCK_STYLE_WITHOUT"] = "����";
$MESS["LANDING_BLOCK_STYLE_OPACITY"] = "������������";
$MESS["LANDING_BLOCK_STYLE_PADDING"] = "���������� ������";
$MESS["LANDING_BLOCK_STYLE_HEIGHT_VH"] = "������ (% �� ������ ������)";
$MESS["LANDING_BLOCK_STYLE_PADDING_TOP"] = "���������� ������ ������";
$MESS["LANDING_BLOCK_STYLE_PADDING_BOTTOM"] = "���������� ������ �����";
$MESS["LANDING_BLOCK_STYLE_PADDING_LEFT"] = "���������� ������ �����";
$MESS["LANDING_BLOCK_STYLE_PADDING_RIGHT"] = "���������� ������ ������";
$MESS["LANDING_BLOCK_STYLE_MARGIN_TOP"] = "������� ������ ������";
$MESS["LANDING_BLOCK_STYLE_HEIGHT"] = "������";
$MESS["LANDING_STYLE_DEFAULT"] = "�� ���������";
$MESS["LANDING_STYLE_UPPERCASE"] = "���������";
$MESS["LANDING_STYLE_LOWERCASE"] = "��������";
$MESS["LANDING_STYLE_UNDERLINE"] = "������������";
$MESS["LANDING_STYLE_STRIKE"] = "�����������";
$MESS["LANDING_BLOCK_STYLE_COLUMNS_ON_ROW"] = "������� � ����� ������";
$MESS["LANDING_BLOCK_STYLE_DEVICE_DISPLAY"] = "��������� �� �����������";
$MESS["LANDING_TEXT_STYLE_WITHOUT_SHADOW"] = "��� ����";
$MESS["LANDING_BLOCK_STYLE_WITHOUT_SHADOW"] = "��� ����";
$MESS["LANDING_BLOCK_STYLE_BORDER_RADIUS"] = "������ ����������";
$MESS["LANDING_BLOCK_STYLE_BORDER_WIDTH"] = "������� �����";
$MESS["LANDING_BLOCK_STYLE_BACKGROUND_HOVER"] = "��� ��� ���������";
$MESS["LANDING_BLOCK_STYLE_COLOR_HOVER"] = "���� ��� ���������";
$MESS["LANDING_BLOCK_STYLE_BACKGROUND_OVERLAY"] = "���� ���������";
$MESS["LANDING_BLOCK_STYLE_COLOR_BUTTON"] = "���� ������ (��� �����)";
$MESS["LANDING_BLOCK_STYLE_COLOR_BUTTON_OUTLINE"] = "���� ����� (������ �� ����� ����)";
$MESS["LANDING_BLOCK_STYLE_ANIMATION"] = "�������� ���������";
$MESS["LANDING_BLOCK_STYLE_ANIMATION_NONE"] = "��� ��������";
$MESS["LANDING_BLOCK_STYLE_BG_COLOR_BEFORE"] = "���� ��������";
$MESS["LANDING_BLOCK_STYLE_BG_COLOR_BEFORE"] = "����";

$MESS["LANDING_STYLE_AUTO"] = "����";

//$MESS["LANDING_BLOCK_STYLE_SOCIAL"] = "�������";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__TWITTER"] = "Twitter";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__SKYPE"] = "Skype";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__FACEBOOK"] = "Facebook";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__PINTEREST"] = "Pinterest";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__VINE"] = "Vine";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__YOUTUBE"] = "Youtube";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__GOOGLE-PLUS"] = "Google-plus";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__DRIBBBLE"] = "Dribbble";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__VK"] = "Vk";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__INSTAGRAM"] = "Instagram";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__LINKEDIN"] = "LinkedIn";
//$MESS["LANDING_BLOCK_STYLE_SOCIAL__ODNOKLASSNIKI"] = "Odnoklassniki";